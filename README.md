ics-ans-role-awx
===================

Ansible role to install [Ansible AWX](https://github.com/ansible/awx).

The role supports using external services for PostgreSQL, RabbitMQ and
Memcached. Setting the variables 'awx_pg_host', 'awx_mq_host' and
'awx_memcached_host' respectively, will skip the creating of docker
containers for that service and use the host variable instead.

The variable 'awx_settings' can be used to supply any part of the
settings slugs found in <awx_api_uri>/settings.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
# PostgreSQL
awx_pg_image: postgres:9.6
awx_pg_data_dir: /var/awx/data
awx_pg_username: awx
awx_pg_password: awx
awx_pg_database: awx
awx_pg_port: 5432
awx_pg_container_name: awx_postgres

# RabbitMQ
awx_mq_image: rabbitmq:3
awx_mq_user: guest
awx_mq_password: guest
awx_mq_port: 5672
awx_mq_vhost: awx
awx_mq_container_name: awx_rabbitmq

# Memcached
awx_memcached_port: 11211
awx_memcached_container_name: awx_memcached

# AWX Web
awx_web_container_name: awx_web
awx_web_image: ansible/awx_web:6.1.0
awx_web_hostname: "{{ ansible_fqdn }}"
awx_api_version: v2
awx_api_uri: https://{{ awx_web_hostname }}/api/{{ awx_api_version }}
awx_web_frontend_rule: "Host:{{ ansible_fqdn }}"

# AWX Task
awx_task_container_name: awx_task
awx_task_image: ansible/awx_task:6.1.0

# AWX configuration
awx_admin_user: admin
awx_admin_password: password
awx_secret_key:
awx_settings:
  system:
    TOWER_URL_BASE: "https://{{ awx_web_hostname }}"

# Traefik
awx_network: awx-network
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-awx
```

License
-------

BSD 2-clause
