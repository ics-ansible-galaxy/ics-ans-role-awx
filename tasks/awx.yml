---
- name: Create AWX configuration directory
  file:
    path: "{{ awx_web_host_conf_dir }}"
    state: directory
    mode: 0600

- name: Set secret key
  copy:
    content: "{{ awx_secret_key }}"
    dest: "{{ awx_web_host_conf_dir }}/SECRET_KEY"
    mode: 0600

- name: Create configuration files
  template:
    src: "{{ item }}.j2"
    dest: "{{ awx_web_host_conf_dir }}/{{ item }}"
    mode: 0600
  loop:
    - environment.sh
    - credentials.py
  notify: Restart AWX

- name: Get data migrations status
  docker_container:
    name: awx_showmigrations
    image: "{{ awx_task_image }}"
    user: root
    detach: false
    cleanup: true
    purge_networks: true
    networks:
      - name: "{{ awx_network }}"
    command: awx-manage showmigrations
    volumes: "{{ awx_container_volumes + awx_extra_container_volumes }}"
  register: showmigrations
  changed_when: false

- name: List existing containers
  command: "docker ps --format {{ '{{.Image}}' }}"  # noqa 206
  register: existing_containers
  changed_when: false

- name: Stop AWX task container before data migration
  docker_container:
    name: awx_task
    state: stopped
  when:
    - awx_task_container_name in existing_containers.stdout
    - showmigrations.ansible_facts.docker_container.Output | regex_findall('^ \\[ \\].*', multiline=True)

- name: Stop AWX web container before data migration
  docker_container:
    name: awx_web
    state: stopped
  when:
    - awx_web_container_name in existing_containers.stdout
    - showmigrations.ansible_facts.docker_container.Output | regex_findall('^ \\[ \\].*', multiline=True)

- name: Perform data migration
  docker_container:
    name: awx_migrate
    image: "{{ awx_task_image }}"
    user: root
    detach: false
    purge_networks: true
    networks:
      - name: "{{ awx_network }}"
    command: awx-manage migrate
    volumes: "{{ awx_container_volumes + awx_extra_container_volumes }}"
  register: migration
  when: showmigrations.ansible_facts.docker_container.Output | regex_findall('^ \\[ \\].*', multiline=True)

- name: Activate AWX Web Container
  docker_container:
    name: "{{ awx_web_container_name }}"
    hostname: awxweb
    state: started
    restart_policy: always
    image: "{{ awx_web_image }}"
    user: root
    purge_networks: true
    networks:
      - name: "{{ awx_network }}"
        aliases:
          - awxweb
      - name: "{{ traefik_network }}"
    exposed_ports:
      - "8052"
    volumes: "{{ awx_container_volumes + awx_extra_container_volumes }}"
    env:
      http_proxy: "{{ http_proxy | default('') }}"
      https_proxy: "{{ https_proxy | default('') }}"
      no_proxy: "{{ no_proxy | default('') }}"
      SECRET_KEY: "{{ awx_secret_key }}"
    labels:
      traefik.enable: "true"
      traefik.backend: "awx_web"
      traefik.port: "8052"
      traefik.frontend.rule: "{{ awx_web_frontend_rule }}"
      traefik.docker.network: "{{ traefik_network }}"

- name: Create ssh directory
  file:
    path: "{{ awx_task_ssh_key_file | dirname }}"
    state: directory
    mode: 0700

- name: Generate ssh key pair
  command: "ssh-keygen -b 2048 -t rsa -f {{ awx_task_ssh_key_file }} -q -N '' -C 'awx_task@{{ ansible_fqdn }}'"
  args:
    creates: "{{ awx_task_ssh_key_file }}"

- name: Get public key
  slurp:
    src: "{{ awx_task_ssh_key_file }}.pub"
  register: public_key

- name: Print public key
  debug:
    msg: "{{ public_key }}"

- name: Activate AWX Task Container
  docker_container:
    name: "{{ awx_task_container_name }}"
    hostname: awx
    state: started
    restart_policy: always
    image: "{{ awx_task_image }}"
    user: root
    command: /usr/bin/launch_awx_task.sh
    purge_networks: true
    networks:
      - name: "{{ awx_network }}"
    env:
      http_proxy: "{{ http_proxy | default('') }}"
      https_proxy: "{{ https_proxy | default('') }}"
      no_proxy: "{{ no_proxy | default('') }}"
    volumes: "{{ awx_container_volumes + awx_extra_container_volumes + awx_task_container_volumes }}"

- name: Remove obsolete containers
  docker_container:
    name: "{{ item }}"
    state: absent
  loop:
    - awx_memcached
    - awx_rabbitmq

- name: Verify that the AWX task instance is started and registered
  uri:
    url: "{{ awx_api_uri }}/instance_groups/1/"
    user: "{{ awx_admin_user }}"
    password: "{{ awx_admin_password }}"
    force_basic_auth: true
    validate_certs: false
    follow_redirects: none
    return_content: true
    status_code: 200
    body_format: json
  register: result
  until: result is succeeded and result.json.instances > 0
  # Try every 3 secs for 10 minutes
  retries: 400
  delay: 3

- name: Configure AWX
  uri:
    url: "{{ awx_api_uri }}/settings/{{ item.key }}/"
    method: PATCH
    user: "{{ awx_admin_user }}"
    password: "{{ awx_admin_password }}"
    body: "{{ item.value | to_json }}"
    force_basic_auth: true
    body_format: json
    validate_certs: false
    follow_redirects: none
  with_dict: "{{ awx_settings }}"
