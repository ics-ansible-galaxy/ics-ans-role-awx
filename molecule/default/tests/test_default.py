import os
import json
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_awx_containers(host):
    with host.sudo():
        cmd = host.run(r"set +x; docker ps --format \{\{.Names\}\}")
    assert cmd.rc == 0
    # Get the names of the running containers
    names = cmd.stdout.strip().splitlines()
    print(names)
    assert all(name in names for name in ['awx_redis', 'awx_postgres', 'awx_task', 'awx_web', 'traefik_proxy'])


def test_awx_api(host):
    # This tests that traefik forwards traffic to the AWX web server
    # and that we can access the AWX REST api
    cmd = host.run('curl -k https://ics-ans-role-awx-default/api/v2/ping/')
    ping = json.loads(cmd.stdout)
    assert len(ping['instance_groups']) > 0 and len(ping['instance_groups'][0]['instances']) > 0
